# lkft-stable-report

This is a squad based report for Linux stable branches

## How does it work

1. At 'scan' stage it gets the list of all the projects from SQUAD and builds a
   job queue for the latter stages in 'jobs' subdirectory. Those projects that
   have no new builds will have respective summary files created in 'summaries'
   subdir.
2. At 'generate' stage (done in several instances in parallel) each respective
   report is generated in an artifact subdirectory named 'reports' with
   file name containing project slug as well as build version. Generated file
   is the RFC standard email file (.eml) that can be processed using existing
   software. Besides the above a summary file is created in 'summaries' subfolder
   containing a brief information about the report just generated.
4. At 'notify' stage all the summaries under 'sommaries' subdirectory are
   joined into an email and that email is sent to preconfigured addressees.
5. At 'send' stage (manually triggered) all the reports are sent to their
   respective addresees.


Scan creates a job directory with job files, if you specify specific triplets, it verifies that they exists.
Generate takes the job files and take into acount -s -e flags
Notify combining notifications files into an email.
Send - should send reports (not deployed).

## Examples
python3 stable_report.py list -j lkft:linux-stable-rc-linux-5.10.y:
python3 stable_report.py scan generate notify send -j lkft:linux-stable-rc-linux-5.4.y:,, -s ltp
python3 stable_report.py scan generate notify send -j lkft:linux-stable-rc-linux-5.4.y:,, -s ltp-smoke


python3 stable_report.py scan generate notify send -c -j lkft:linux-stable-rc-linux-5.4.y:,, -s ltp

python3 stable_report.py scan generate notify send -c -j lkft:linux-stable-rc-linux-5.4.y:v5.4.97,, -s ltp
