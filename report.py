#!/usr/bin/python3

# import boto3
import argparse
import os
import os.path
import pprint
import sys

from squad_client.core.api import SquadApi
from squad_client.core.models import Squad

from utils.builds import scan_builds, list_builds
from utils.reports import generate_reports, send_reports, send_notifications
from utils.reports import send_email
from utils.parsing import parse_job_definitions

DEBUG = False
# DEBUG=False

if DEBUG:
    import requests_cache

    requests_cache.install_cache()

"""LKFT Stable Report

Environment variables accepted
------------------------------

    AWS_MODE            - to be set when deployed in AWS (any value will do)
    AWS_DYNAMODB_REGION - region to be used for AWS's dynamodb
    AWS_DYNAMODB_TABLEN - name of the table to be used in dynamodb
    LKFT_SCAN_DEPTH     - how many already scanned builds to check when
                          looking for skipped ones
    LKFT_PROJECT_LIST   - list of projects (and, possibly, builds) to process;
                          if not specified - pattern matching is used
                          against the list of projects in squad and builds
                          are checked against a dynamodb table
"""


# group:project:build,build,build;group:project;group:project:build,group:project:build,build

# list of groups, projects (and exact builds) to process
# the format is:
#   <job-definitions> ::= <job-definition> | ";" <job-definitions>
#   <job-definition>  ::= <first-triplet> "," <triplet> "," <triplet>
#                       | <first-triplet> "," <triplet>
#                       | <first-triplet> ;
#   <first-triplet>   ::= <group> ":" <project> ":" <build>
#                       | <group> ":" <project>
#                       | <group> ;
#   <triplet>         ::= <group> ":" <project> ":" <build>
#                       | <project> ":" <build>
#                       | <build> ;
#   <group>           ::= <name> ;
#   <project>         ::= <name> ;
#   <build>           ::= <name> ;
#
# if not set - all the projects matching config.REGEXP_STABLE_PROJECT will be processed
# and builds that have not yet been reported will be processed, automatically finding the suitable previous and base builds


def unique_from_sorted(a_list):
    result = []
    for item in a_list:
        if result and result[-1] == item:
            continue
        result.append(item)

    return result


def list_suites(squad, args):
    """
    List all the suites.
    """

    success, jobs = parse_job_definitions(squad, args, True)
    if not success:
        return 1

    if not jobs:
        suites = squad.suites(count=-1)
        suites = unique_from_sorted(sorted([suite.slug for suite in suites.values()]))
        print("Suites ({0}):".format(len(suites)))
        for suite in suites:
            if suite.strip() == "":
                continue
            print(" - {0}".format(suite))

        return 0

    for job_id, job in enumerate(jobs):
        triplet = job.get("build")

        group = triplet.get("group")
        project = triplet.get("project")
        build = triplet.get("build")

        if not project or project == "?":
            suites = {}
            projects = group.projects(count=-1)
            for project in projects.values():
                project_suites = project.suites(count=-1)
                suites.update(project_suites)

            suites = unique_from_sorted(
                sorted([suite.slug for suite in suites.values()])
            )
            print("Suites for the group {0} ({1}):".format(group.slug, len(suites)))
            for suite in suites:
                if suite.strip() == "":
                    continue
                print(" - {0}".format(suite))

            return 0

        if not build or build == "?":
            suites = project.suites(count=-1)
            suites = unique_from_sorted(
                sorted([suite.slug for suite in suites.values()])
            )
            print(
                "Suites for the project {0}:{1} ({2}):".format(
                    group.slug, project.slug, len(suites)
                )
            )
            for suite in suites:
                if suite.strip() == "":
                    continue
                print(" - {0}".format(suite))

            return 0

        suites = {}
        idx = 0
        tests = build.tests(count=-1)
        for test in tests.values():
            suites[idx] = suite_name_from_url(squad, test.suite)
            idx += 1

        suites = unique_from_sorted(sorted([suite for suite in suites.values()]))
        print(
            "Suites for the build {0}:{1}:{2} ({3}):".format(
                group.slug, project.slug, build.version, len(suites)
            )
        )
        for suite in suites:
            if suite.strip() == "":
                continue
            print(" - {0}".format(suite))

    return 0


def list_environments(squad, args):
    """
    List all the environments.
    """

    success, jobs = parse_job_definitions(squad, args, True)
    if not success:
        return 1

    if not jobs:
        environments = squad.environments(count=-1)
        environments = unique_from_sorted(
            sorted([env.slug for env in environments.values()])
        )
        print("Environments for the server ({0}):".format(len(environments)))
        for environment in environments:
            if environment.strip() == "":
                continue
            print(" - {0}".format(environment))

        return 0

    for job_id, job in enumerate(jobs):
        triplet = job.get("build")

        group = triplet.get("group")
        project = triplet.get("project")
        build = triplet.get("build")

        if not project or project == "?":
            environments = {}
            projects = group.projects(count=-1)
            for project in projects.values():
                project_environments = project.environments(count=-1)
                environments.update(project_environments)

            environments = unique_from_sorted(
                sorted([env.slug for env in environments.values()])
            )
            print(
                "Environments for the group {0} ({1}):".format(
                    group.slug, len(environments)
                )
            )
            for environment in environments:
                if environment.strip() == "":
                    continue
                print(" - {0}".format(environment))

            return 0

        if not build or build == "?":
            environments = project.environments(count=-1)
            environments = unique_from_sorted(
                sorted([env.slug for env in environments.values()])
            )
            print(
                "Environments for the project {0}:{1} ({2}):".format(
                    group.slug, project.slug, len(environments)
                )
            )
            for environment in environments:
                if environment.strip() == "":
                    continue
                print(" - {0}".format(environment))

            return 0

        environments = {}
        idx = 0
        tests = build.tests(count=-1)
        for test in tests.values():
            environments[idx] = environment_name_from_url(squad, test.environment)
            idx += 1

        environments = unique_from_sorted(
            sorted([env for env in environments.values()])
        )
        print(
            "Environments for the build {0}:{1}:{2} ({3}):".format(
                group.slug, project.slug, build.version, len(environments)
            )
        )
        for environment in environments:
            if environment.strip() == "":
                continue
            print(" - {0}".format(environment))

    return 0


def dump_object(obj, margin="", escape_nls=True):
    functions = []
    for var_name in dir(obj):
        if var_name.startswith("_"):
            continue

        value = getattr(obj, var_name)
        if type(value).__name__ == "method" or type(value).__name__ == "function":
            functions.append(var_name)
            continue

        if type(value) == str and escape_nls:
            value = value.replace("\r", "").replace("\n", "↵")

        print(
            "{0} - '{1}': [{2}] '{3}'".format(
                margin, var_name, type(value).__name__, value
            )
        )
    if functions:
        print("{0} - functions: {1}".format(margin, ", ".join(functions)))


def debug(squad, args):
    from utils.parsing import parse_job_definitions

    success, jobs = parse_job_definitions(squad, args)

    print("Jobs:")
    pprint.PrettyPrinter(indent=2).pprint(jobs)


g_stages = {
    "scan": scan_builds,
    "generate": generate_reports,
    "notify": send_notifications,
    "send": send_reports,
    "list": list_builds,
    "slist": list_suites,
    "elist": list_environments,
    "debug": debug,
}

# Variants of build definitions
#
# Legend:
# '  ' - not provided, e.g. 'aa:bb:cc,' - no triplet for the second position
# '**' - any value or missing
# '++' - provided
# '--' - skipped, e.g. in 'aa:bb:' third triplet is skipped
#
#           triplets
#  first     second    third
# gr pr bl  gr pr bl  gr pr bl  description
# ========  ========  ========  =============================
#           ********  ********  error: missing group and project
# ++ -- --  ********  ********  error: missing project
# ++ ++ --                      report the most recent build, no comparison
# ++ ++ --        --        --  same, but compare to the previous and base builds
# ++ ++ --     ++ --        --  same, but compare to another project's last and base builds
# ++ ++ --  ++ ++ --        --  same, but compare to another group and project's last and base builds
# ++ ++ ++  ++ ++ ++  ++ ++ ++  explicit comparison of three fully qualified builds
#
# Examples:
#  "lkft" -> {'group': 'lkft'}
#  "lkft:linux-stable-rc-linux-5.10.y" -> {'group': 'lkft', 'project': 'linux-stable-rc-linux-5.10.y'}
#  "lkft:linux-stable-rc-linux-5.10.y:" -> {'group': 'lkft', 'project': 'linux-stable-rc-linux-5.10.y', 'build': '?'}


def main():
    global g_stages

    ap = argparse.ArgumentParser(description="LKFT stable report")
    ap.add_argument(
        "-j",
        "--job",
        dest="jobs",
        action="append",
        type=str,
        help="jobs to process",
    )
    ap.add_argument(
        "-s",
        "--suite",
        dest="suites",
        action="append",
        default=[],
        type=str,
        help="test suites to process",
    )
    ap.add_argument(
        "-e",
        "--environment",
        dest="environments",
        action="append",
        default=[],
        type=str,
        help="test environments to process",
    )
    ap.add_argument(
        "-a",
        "--all",
        dest="all",
        action="store_true",
        default=False,
        help="process all builds, including the unfinished ones",
    )
    ap.add_argument(
        "stages",
        metavar="stage",
        type=str,
        nargs="+",
        choices=g_stages.keys(),
        help="mention which stage(s) to run, one or more of: {0}".format(
            ", ".join(g_stages.keys())
        ),
    )

    args = ap.parse_args()

    if len(args.stages):
        SquadApi.configure(url=os.getenv("QA_REPORTS_SERVER","https://qa-reports.linaro.org/"))
        squad = Squad()
        for stage in args.stages:
            function = g_stages.get(stage)
            if not function:
                print("Unknown stage: {0}, skipped".format(stage))
                continue

            result = function(squad, args)
            if result:
                return result

    return 0


if __name__ == "__main__":
    sys.exit(main())
