import re
from utils.env_vars import REGEXP_ENVIRONMENT_URL, REGEXP_SUITE_URL

# in memory cache of suite names retrieved by suite id from suite urls
g_suite_name_by_url = {}
# in memory cache of environment names retrieved by environment id from environment urls
g_environment_name_by_url = {}


def suite_name_from_url(project, url):
    global g_suite_name_by_url

    lookup_result = g_suite_name_by_url.get(url)
    if lookup_result:
        return lookup_result

    match = re.match(REGEXP_SUITE_URL, url)
    if not match:
        suite_name = url
    else:
        suite = project.suites(id=match.group("id"))
        if suite:
            suite = list(suite.values())[0]
            if suite:
                suite_name = suite.slug
            else:
                suite_name = "suite-{0}".format(match.group("id"))
        else:
            suite_name = "suite-{0}".format(match.group("id"))

    g_suite_name_by_url[url] = suite_name

    return suite_name


def environment_name_from_url(squad, url):
    global g_environment_name_by_url

    lookup_result = g_environment_name_by_url.get(url)
    if lookup_result:
        return lookup_result

    match = re.match(REGEXP_ENVIRONMENT_URL, url)
    if not match:
        environment_name = url
    else:
        environment = squad.environments(id=match.group("id"))
        if environment:
            environment = list(environment.values())[0]
            if environment:
                environment_name = environment.slug
            else:
                environment_name = "environment-{0}".format(match.group("id"))
        else:
            environment_name = "environment-{0}".format(match.group("id"))

    g_environment_name_by_url[url] = environment_name

    return environment_name
